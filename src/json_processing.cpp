#include "json_processing.h"

std::string JSONParser::map_to_str(const object_type &data){
    std::ostringstream ss;
    ss << "{";
    bool first = true;
    for (const auto& pair : data) {
        if (!first) {
            ss << ",";
        }
        ss << "\"" << pair.first << "\":"  << pair.second.as_str();
        first = false;
    }
    ss << "}";
    return ss.str();
}

std::string JSONParser::array_to_str(const array_type &data){
    std::ostringstream ss;
    ss << "[";
    for(const auto& json_val: data){
        ss << json_val.as_str();
        ss << ",";
    }
    std::string to_return = ss.str();
    to_return.back() = ']'; // replace additional ',' with ']'
    return to_return;

}

std::string JSONValue::as_str() const{
    switch (value_type_) {
        case VALUE_TYPE::INT:
            return std::to_string(int_val_);
        case VALUE_TYPE::FLOAT:
            return std::to_string(float_val_);
        case VALUE_TYPE::BOOL:
            if(bool_val_) return std::string{"true"};
            else return std::string{"false"};
        case VALUE_TYPE::STRING:
            return "\"" + string_val_ + "\"";
        case VALUE_TYPE::OBJECT:
            return JSONParser::map_to_str(object_val_);
        case VALUE_TYPE::ARRAY:
            return JSONParser::array_to_str(array_val_);
    }
    throw;
}

float JSONValue::as_float() const{
    switch (value_type_) {
        case VALUE_TYPE::INT:
            return (float)int_val_;
        case VALUE_TYPE::FLOAT:
            return float_val_;
        case VALUE_TYPE::BOOL:
            if(bool_val_) return 1;
            else return 0;
        case VALUE_TYPE::STRING:
        case VALUE_TYPE::OBJECT:
        case VALUE_TYPE::ARRAY:
            throw;
    }
    throw;
}

long JSONValue::as_int() const{
    switch (value_type_) {
        case VALUE_TYPE::INT:
            return int_val_;
        case VALUE_TYPE::FLOAT:
            return (long)std::floor(float_val_);
        case VALUE_TYPE::BOOL:
            if(bool_val_) return 1;
            else return 0;
        case VALUE_TYPE::STRING:
        case VALUE_TYPE::OBJECT:
        case VALUE_TYPE::ARRAY:
            throw;
    }
    throw;
}

bool JSONValue::as_bool() const{
    switch (value_type_) {
        case VALUE_TYPE::INT:
            return (bool)int_val_;
        case VALUE_TYPE::FLOAT:
            return (bool)float_val_;
        case VALUE_TYPE::BOOL:
            return bool_val_;
        case VALUE_TYPE::STRING:
        case VALUE_TYPE::OBJECT:
        case VALUE_TYPE::ARRAY:
            throw;
    }
    throw;
}

object_type JSONValue::as_map() const{
    switch (value_type_) {
        case VALUE_TYPE::OBJECT:
            return object_val_;
        case VALUE_TYPE::INT:
        case VALUE_TYPE::FLOAT:
        case VALUE_TYPE::BOOL:
        case VALUE_TYPE::STRING:
        case VALUE_TYPE::ARRAY:
            throw;
    }
    throw;
}


array_type JSONValue::as_array() const {
    if(value_type_ == VALUE_TYPE::ARRAY) return array_val_;
    else throw;
}

JSONValue JSONValue::operator[](const std::string& key) const{
    if(value_type_ == VALUE_TYPE::OBJECT) return object_val_.at(key);
    else throw;
}


void JSONParser::remove_whitespaces(std::string &data){
    data.erase(std::remove_if(data.begin(),
                              data.end(),
                              [](unsigned char x){return std::isspace(x);}),
               data.end());
}

void JSONParser::construct_new_entry(std::ostringstream &key_buffer, std::ostringstream &val_buffer, object_type &write_target){
    std::string key_str = key_buffer.str();
    if(key_str.empty()) return;
    key_str = key_str.substr(1, key_str.length()-2); //remove double quotes
    key_buffer.str("");
    std::string val_str = val_buffer.str();
    if(val_str.empty()) return;
    val_buffer.str("");
    write_target.emplace(key_str, extract_value(val_str));
}

object_type JSONParser::parse_json(std::string json){
    object_type to_return{};
    // no whitespaces keys are allowed
    remove_whitespaces(json);
    json = json.substr(1, json.length()-2);
    unsigned int square_bracket_counter = 0;
    unsigned int curly_bracket_counter = 0;
    std::ostringstream new_key{}, new_val{};
    bool collecting_key = true;
    bool finished_objects_and_arrays = false;
    for(const char character: json){
        if(character == ':' && collecting_key){
            collecting_key = false;
            continue;
        }
        if(character == ',' && square_bracket_counter == 0 && curly_bracket_counter == 0 && !finished_objects_and_arrays) {
            construct_new_entry(new_key, new_val, to_return);
            collecting_key = true;
            continue;
        }

        if(finished_objects_and_arrays){ // set to false after skipping default write function
            finished_objects_and_arrays = false;
            if(character == ',') continue;
        }

        if(collecting_key) new_key << character;
        else{ //equal to collecting val
            new_val << character;
            switch (character) { //don't expect such characters in keys
                case '[':
                    square_bracket_counter++;
                    break;
                case ']':
                    square_bracket_counter--;
                    if(square_bracket_counter == 0 && curly_bracket_counter == 0){
                        construct_new_entry(new_key, new_val, to_return);
                        collecting_key = true;
                        finished_objects_and_arrays = true;
                    }
                    break;
                case '{':
                    curly_bracket_counter++;
                    break;
                case '}':
                    curly_bracket_counter--;
                    if(square_bracket_counter == 0 && curly_bracket_counter == 0){
                        construct_new_entry(new_key, new_val, to_return);
                        collecting_key = true;
                        finished_objects_and_arrays = true;
                    }
                    break;
                default:
                    break;
            }
        }
    }

    construct_new_entry(new_key, new_val, to_return);

    return to_return;
}

array_type JSONParser::create_array(std::string array_str){
    array_type to_return{};
    remove_whitespaces(array_str);
    array_str = array_str.substr(1, array_str.length() - 2);
    unsigned int square_bracket_counter = 0;
    unsigned int curly_bracket_counter = 0;
    std::ostringstream new_object_buffer{};

    for(const char character: array_str){
        if(character == ',' && square_bracket_counter == 0 && curly_bracket_counter == 0){
            std::string extracted_value = new_object_buffer.str();
            new_object_buffer.str("");
            to_return.push_back(extract_value(extracted_value));
            continue;
        }
        new_object_buffer << character;
        switch (character) {
            case '[':
                square_bracket_counter++;
                break;
            case ']':
                square_bracket_counter--;
                break;
            case '{':
                curly_bracket_counter++;
                break;
            case '}':
                curly_bracket_counter--;
                break;
            default:
                break;
        }
    }

    std::string extracted_value = new_object_buffer.str();
    if(extracted_value.empty()) return to_return;
    to_return.push_back(extract_value(extracted_value));

    return to_return;

}

JSONValue JSONParser::extract_value(std::string &val_str){
    //std::cout <<"Front val" <<val_str.front() <<std::endl;
    if (val_str == "true") {
        return JSONValue(true);
    } else if (val_str == "false") {
        return JSONValue(false);
    } else if (isdigit(val_str[0]) || val_str[0] == '-') {
        if (val_str.find('.') != std::string::npos) {
            return JSONValue(std::stof(val_str));
        } else {
            return JSONValue(std::stol(val_str));
        }
    } else if (val_str.front() == '"') {
        std::string wo_quotes = val_str.substr(1, val_str.length() - 2);
        return JSONValue(wo_quotes);
    } else if (val_str.front() == '{') {
        object_type new_object = parse_json(val_str);
        return JSONValue(new_object);
    } else if (val_str.front() == '['){
        array_type new_object = create_array(val_str);
        return JSONValue(new_object);
    }
    throw std::runtime_error("Cannot extract json value");
}
