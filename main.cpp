#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include "nlohmann/json.hpp"
#include <sstream>

#include <thread>
#include <chrono>

// Dla wygody używamy przestrzeni nazw z biblioteki nlohmann::json
using json = nlohmann::json;

int main() {
    while (true) {
        // Otwieramy plik JSON
        std::ifstream file("data.json");
        
        // Sprawdzamy, czy plik został otwarty
        if (!file.is_open()) {
            std::cerr << "Nie można otworzyć pliku!" << std::endl;
            return 1;
        }
        
        // Wczytujemy zawartość pliku do obiektu JSON
        json jsonData;
        file >> jsonData;
        
        // Zamykanie pliku
        file.close();
        
        // Odczytujemy dane z obiektu JSON
        double czestotliwosc = jsonData["czestotliwosc"];
        int produkcja_cala = jsonData["produkcja_cala"];
        int produkcja_cieplne = jsonData["produkcja_cieplne"];
        int produkcja_fotowoltaika = jsonData["produkcja_fotowoltaika"];
        int produkcja_wiatr = jsonData["produkcja_wiatr"];
        int produkcja_wodna = jsonData["produkcja_wodna"];
        int64_t timestamp = jsonData["timestamp"];
        int zapotrzebowanie = jsonData["zapotrzebowanie"];
        
        // Wyświetlamy dane
        std::cout << "Czestotliwosc: " << czestotliwosc << std::endl;
        std::cout << "Produkcja cala: " << produkcja_cala << std::endl;
        std::cout << "Produkcja cieplne: " << produkcja_cieplne << std::endl;
        std::cout << "Produkcja fotowoltaika: " << produkcja_fotowoltaika << std::endl;
        std::cout << "Produkcja wiatr: " << produkcja_wiatr << std::endl;
        std::cout << "Produkcja wodna: " << produkcja_wodna << std::endl;
        std::cout << "Timestamp: " << timestamp << std::endl;
        std::cout << "Zapotrzebowanie: " << zapotrzebowanie << std::endl;

        // Otwieramy plik CSV w trybie do dopisywania
        std::ofstream csvFile("data.csv", std::ios::out | std::ios::app);
        
        // Sprawdzamy, czy plik został otwarty
        if (!csvFile.is_open()) {
            std::cerr << "Nie można otworzyć pliku CSV do zapisu!" << std::endl;
            return 1;
        }

        // Sprawdzamy, czy plik jest pusty, aby dodać nagłówki tylko w przypadku nowego pliku
        std::ifstream checkFile("data.csv");
        checkFile.seekg(0, std::ios::end);
        bool isEmpty = (checkFile.tellg() == 0);
        checkFile.close();
        
        // Zapisujemy nagłówki do pliku CSV, jeśli plik jest pusty
        if (isEmpty) {
            csvFile << "czestotliwosc,produkcja_cala,produkcja_cieplne,produkcja_fotowoltaika,produkcja_wiatr,produkcja_wodna,timestamp,zapotrzebowanie\n";
        }
        
        // Zapisujemy dane do pliku CSV
        csvFile << czestotliwosc << ','
                << produkcja_cala << ','
                << produkcja_cieplne << ','
                << produkcja_fotowoltaika << ','
                << produkcja_wiatr << ','
                << produkcja_wodna << ','
                << timestamp << ','
                << zapotrzebowanie << '\n';
        
        // Zamykanie pliku CSV
        csvFile.close();
        
        // Czekamy 10 sekund przed następnym odczytem
        std::this_thread::sleep_for(std::chrono::seconds(10));
    }
    
    return 0;
}

