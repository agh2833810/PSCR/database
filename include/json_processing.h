#ifndef WEB_SCRAPING_JSON_PROCESSING_H
#define WEB_SCRAPING_JSON_PROCESSING_H
#include <string>
#include <algorithm>
#include <cctype>
#include <map>
#include <iostream>
#include <cmath>
#include <utility>
#include <sstream>
#include <vector>

enum class VALUE_TYPE{
    INT,
    FLOAT,
    BOOL,
    STRING,
    OBJECT,
    ARRAY
};

class JSONValue;
using object_type = std::map<std::string, JSONValue>;
using array_type = std::vector<JSONValue>;

class JSONValue{
public:

    JSONValue() = delete;

    [[maybe_unused]] explicit JSONValue(std::string &x): value_type_(VALUE_TYPE::STRING), string_val_(x) {};
    [[maybe_unused]] explicit JSONValue(const char* x): value_type_(VALUE_TYPE::STRING), string_val_(x) {};
    [[maybe_unused]] explicit JSONValue(long x): value_type_(VALUE_TYPE::INT), int_val_(x) {};
    [[maybe_unused]] explicit JSONValue(float x): value_type_(VALUE_TYPE::FLOAT), float_val_(x) {};
    [[maybe_unused]] explicit JSONValue(bool x): value_type_(VALUE_TYPE::BOOL), bool_val_(x) {};
    [[maybe_unused]] explicit JSONValue(object_type x): value_type_(VALUE_TYPE::OBJECT), object_val_(x) {};
    [[maybe_unused]] explicit JSONValue(array_type x): value_type_(VALUE_TYPE::ARRAY), array_val_(x) {};

    std::string as_str() const;
    float as_float() const;
    long as_int() const;
    bool as_bool() const;
    object_type as_map() const;
    array_type as_array() const;

    JSONValue operator[](const std::string& key) const;

    VALUE_TYPE get_val_type() const {return value_type_;}

private:
    VALUE_TYPE value_type_;
    long int_val_= 0;
    float float_val_= 0;
    std::string string_val_;
    bool bool_val_= false;
    object_type object_val_;
    array_type array_val_;
};

class JSONParser{
public:
    JSONParser(const char* json_txt): text_(json_txt) {object_= parse_json(text_);};
    JSONParser(std::string json_str): text_(json_str) {object_= parse_json(text_);};

    std::string get_unprocessed() const {return text_;}
    std::string as_str() const {return map_to_str(object_);}

    JSONValue operator[](const std::string& key) const {return object_.at(key);}

    static object_type parse_json(std::string json);

    static JSONValue extract_value(std::string &val_str);
    static array_type create_array(std::string array_str);

    static std::string map_to_str(const object_type &data);
    static std::string array_to_str(const array_type &data);
    static void remove_whitespaces(std::string &data);

private:
    std::string text_;
    object_type object_;

    static void construct_new_entry(std::ostringstream &key_buffer, std::ostringstream &val_buffer, object_type &write_target);
};


#endif //WEB_SCRAPING_JSON_PROCESSING_H
